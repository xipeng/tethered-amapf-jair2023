# Tethered-AMAPF-Jair2023

This repo contains the essential code for the project "Non-Crossing Anonymous MAPF for tethered robots".  It consists of two folders, one for the python code including all the geometric and upper bound computations based on the methods proposed in our article, the other is the java code enabling the problem to be solved optimally with Choco Solver.

**Files description:**

In the folder "tethered_mapf/data": we provide all the simulation data we use to carry out the experimental tests.  In the folder "tethered_mapf/test": a test script "test_bound.py" to calculate the various upper bounds, including a simple visualization of the paths found. 

The following test code generates a json file "insance_data.json" in the subfolder "data/json_for_choco", which will be used as input for the Choco solver.

```
def test_optimal_solution():
    # generate json file for choco solver
    display_range = [200, 200]
    map_name = "../data/map/sample10.json"
    obstacles = generate_polygons(map_name)
    simulation_file = "../data/test_alter_10/10_10_0.pickle"
    [x_pts, y_pts] = pickle.load(open(simulation_file, "rb"))
    path_finding(display_range, obstacles, x_pts, y_pts, 1)
```

The  folder "solver_choco" folder contains all the source files implemented in Java. In the "src/Solver.java" file, we show an example of optimal solution calculation: it takes an input file in json format and then generates all the inputs required for the CP model.

**Dependencies:**

In this work, we use Python 3.8 and the Choco version is 10.6. 





