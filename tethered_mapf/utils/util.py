from utils.geometry import *
from pyvisgraph.visgraph import VisGraph
from scipy.optimize import linear_sum_assignment
from utils.schgraph import *
from pyvisgraph.shortest_homotopy_path import path_homotopy
import copy
from utils.bottleneckBM import BottleneckBM, Matching
from utils.bottleneckBM import BMGraph
from utils.vns import SearchVNS, SearchVNSSimple
import func_timeout
import json


def generate_polygons(file):
    # read polygons from json
    with open(file, 'r') as openfile:
        json_object = json.load(openfile)
    polygon_list = list(json_object.values())
    return polygon_list


def generate_vis_graph(polygon_list, display_range):
    polygons = []
    for id_, polygon in enumerate(polygon_list):
        item = []
        for point in polygon:
            tmp = point[1]
            item.append(Point(point[0], display_range[1] - tmp, id_ + 1))
        polygons.append(item)
    g = VisGraph()
    g.build(polygons, status=False)
    return g


def min_weight_match(cost_matrix):
    n = len(cost_matrix)
    row_ind, col_ind = linear_sum_assignment(np.array(cost_matrix))
    solution = [(i, col_ind[i]) for i in range(n)]
    solution.sort(key=lambda e: cost_matrix[e[0]][e[1]])
    return solution


def shortest_configs(x_pts, y_pts, display_range, g):
    cost_matrix = []
    min_weight_edges = {}
    n = len(x_pts)
    start_list = [Point(x_pts[i][0], display_range[1] - x_pts[i][1]) for i in range(n)]
    end_list = [Point(y_pts[j][0],  display_range[1] - y_pts[j][1]) for j in range(n)]
    add_to_visg = g.complete_graph(start_list, end_list)

    for i in range(n):
        start = start_list[i]
        shortest_config = g.djikstra_path(start, end_list, add_to_visg)
        e = []
        for j in range(n):
            min_weight_edges[(i, j)] = [copy.deepcopy(shortest_config[j])]
            e.append(shortest_config[j][1])
        cost_matrix.append(e)
    return [cost_matrix, min_weight_edges]


def homotopy_shorten(path, g, rep_points):
    n = len(path)
    if not check_path_intersect(path):
        # return shortest path
        shortest_config = g.djikstra_single_path(path[0], path[-1])
        return shortest_config[0]
    if n < 3:
        return path
    else:
        h = path_homotopy(path, rep_points)
        new_path = g.homotopic_path(path[0], path[-1], path_length(path), h, rep_points)
        if new_path == []:
            print("error occurs")
            print(path)
        return new_path[0][0]


def remove_crossing(pathset, g, rep_points):
    cross = cross_exist(pathset)
    cnt = 0
    while cross[0] >= 0:
        cnt = cnt +1
        p1 = pathset[cross[1]]
        p2 = pathset[cross[2]]
        if cross[0]==0:
            p1_i = p1[cross[3]]
            p2_i = p1[cross[3]+1]
            p1_j = p2[cross[4]]
            p2_j = p2[cross[4]+1]
            intersection_pt = find_intersect_point(p1_i,p2_i,p1_j,p2_j)
            new_path1 = p1[:cross[3]+1] + [intersection_pt] + p2[cross[4]+1:]
            new_path2 = p2[:cross[4]+1] + [intersection_pt] + p1[cross[3]+1:]
        else:
            new_path1 = p1[:cross[3]+1] + p2[cross[4]+1:]
            new_path2 = p2[:cross[4]+1] + p1[cross[3]+1:]
        pathset[cross[1]] = homotopy_shorten(new_path1, g, rep_points)
        pathset[cross[2]] = homotopy_shorten(new_path2, g, rep_points)
        cross = cross_exist(pathset)
    cross = cross_exist(pathset)
    print("cross: {} , cnt: {}".format(cross[0], cnt))
    return


def compute_lbap_bounds(x_pts, y_pts, min_weight_edges, cost_matrix, g, rep_points):
    '''
    :param x_pts: the starting points
    :param y_pts: the target points
    :param min_weight_edges: the shortest paths between x_pts and y_pts
    :param cost_matrix: cost_matrix[i][j] is the cost of the shortest path between x_pt[i] and y_pt[j]
    :param g: visibility graph
    :param rep_points: reference points to compute the homotopy class
    :return:
    '''
    n = len(x_pts)
    bmg = BMGraph(n, x_pts, y_pts)
    match1 = BottleneckBM(bmg, cost_matrix)
    solution_lb = match1.match()
    inf_bound = cost_matrix[solution_lb[-1][0] - 1][-solution_lb[-1][1] - 1]
    pathset = []
    for i in range(n):
        pathset.append(min_weight_edges[(solution_lb[i][0] - 1, -solution_lb[i][1] - 1)][0][0])

    remove_crossing(pathset, g, rep_points)
    res = search_subgraph(pathset)
    result = []
    for e in res:
        ms = computeSubgraphsMakespan(pathset, e)
        result.append(ms)
    sup_bound_lbap = max(result)
    sup_bound_lbap = math.ceil(sup_bound_lbap)
    print("lower bound : {}, lbap upper bound : {}".format(math.ceil(inf_bound), sup_bound_lbap))
    return math.ceil(inf_bound), sup_bound_lbap


def search_paths(x_pts, y_pts, g, rep_points, display_range, vns_param, generate_json):
    '''
    x_pts:  depart points
    y_pts:  arrival points
    g: visibility graph
    rep_points: reference points to compute the homotopy class
    vns_param: a dictionary of parameters for VNS
    return: a set of paths
    '''
    # calculate distance between anchor points:
    n = len(x_pts)
    assert len(x_pts) == len(y_pts)
    dist_anchor = [[[(x_pts[j][0] - x_pts[i][0]) ** 2 + (x_pts[j][1] - x_pts[i][1]) ** 2, i] for i in range(n)] for j in
                   range(n)]
    for e in dist_anchor:
        e.sort(key=lambda x: x[0])
    sorted_dist_anchor = [[e[1] for e in dist_anchor[i]] for i in range(n)]

    # compute upper bound by lsap
    cost_matrix, min_weight_edges = shortest_configs(x_pts, y_pts, display_range, g)
    solution_lsap = min_weight_match(cost_matrix)
    pathset = []
    for i in range(n):
        pathset.append(min_weight_edges[(solution_lsap[i][0], solution_lsap[i][1])][0][0])
    solution_lsap.sort(key=lambda e: e[0])
    remove_crossing(pathset, g, rep_points)


    res = search_subgraph(pathset)
    result = []
    for e in res:
        ms = computeSubgraphsMakespan(pathset, e)
        result.append(ms)
    sup_bound_lsap = max(result)
    sup_bound_index = res[result.index(sup_bound_lsap)]

    print("lsap bound: {}".format(math.ceil(sup_bound_lsap)))

    # computer the upper bound and lower bound from lbap
    inf_bound, lbap_ub = compute_lbap_bounds(x_pts, y_pts, min_weight_edges, cost_matrix, g, rep_points)

    match = [[] for _ in range(n)]
    crossing_table = np.ones((n * n, n * n), int) * (-1)
    index_list = []
    xpts_index = {}
    ypts_index = {}
    for i in range(n):
        xpts_index[x_pts[i]] = i
        ypts_index[y_pts[i]] = i

    subgraph_index = []
    for path_num, p in enumerate(pathset):
        start = (p[0].x, display_range[1] - p[0].y)
        end = (p[-1].x, display_range[1] - p[-1].y)
        if start in xpts_index.keys():
            start_index = xpts_index[start]
            end_index = ypts_index[end]
        else:
            start_index = ypts_index[start]
            end_index = xpts_index[end]
        if path_num in sup_bound_index:
            subgraph_index.append(start_index)
        match[start_index] = [end_index, 0]
        index_list.append(start_index * n + end_index)
    for i in range(n - 1):
        for j in range(i + 1, n):
            crossing_table[index_list[i], index_list[j]] = 0
            crossing_table[index_list[j], index_list[i]] = 0

    # calculate distance between anchor-destination points
    dist_anchor_dest = [[[min_weight_edges[(j, i)][0][1], i] for i in range(n)] for j in range(n)]
    for e in dist_anchor_dest:
        e.sort(key=lambda x: x[0])
    sorted_dist_anchor_dest = [[0 for i in range(n)] for j in range(n)]
    for i in range(n):
        for j in range(n):
            sorted_dist_anchor_dest[i][dist_anchor_dest[i][j][1]] = j

    kmax = vns_param["kmax"]
    neighbor = vns_param["neighbor"]
    num_path = vns_param["vns2_num_path"]


    # VNS search, combine vns1 + vns2
    # vns1 search
    print("vns1 start")
    vns1 = SearchVNSSimple(g, match, kmax, crossing_table, min_weight_edges, x_pts, y_pts, subgraph_index,
                           math.ceil(sup_bound_lsap), neighbor, 1)
    try:
        vns1.local_search()
    except func_timeout.exceptions.FunctionTimedOut:
        print("vns1 timeout")

    match = [[e[0], e[1]] for e in vns1.solution]
    res = findSubgraphs(n, min_weight_edges, match)
    result = []
    for e in res:
        ms = findSubgraphsMakespan(min_weight_edges, match, e)
        result.append(ms)
    sup_bound_index1 = res[result.index(max(result))]
    sup_bound_vns1 = math.ceil(vns1.sup_bound)
    print("vns2 start")


    # VNS2 search
    crossing_table = [[defaultdict() for _ in range(n * n)] for _ in range(n * n)]
    for i in range(n):
        for j in range(n):
            index1 = (match[i][0]) * n + match[i][1]
            index2 = (match[j][0]) * n + match[j][1]
            if index1 < index2:
                crossing_table[index1][index2][(0, 0)] = [0, 0]
            elif index1 > index2:
                crossing_table[index2][index1][(0, 0)] = [0, 0]


    vns2 = SearchVNS(g, match, kmax, crossing_table, min_weight_edges, x_pts, y_pts, sup_bound_index1,
                     sup_bound_vns1, neighbor, num_path, display_range)


    try:
        vns2.local_search()
    except func_timeout.exceptions.FunctionTimedOut:
        print("vns2 timeout")

    paths = []
    for i in range(n):
        paths.append(vns2.configs[(i, vns2.solution[i][0])][vns2.solution[i][1]][0])
    sup_bound_vns2 = math.ceil(vns2.sup_bound)
    print("vns bound: {}".format(sup_bound_vns2))

    if generate_json:
        ub = sup_bound_vns2
        lb = math.ceil(inf_bound)
        path_ub = ub
        path_lb = lb
        json_file_name = "../data/json_for_choco/instance_data.json"
        generate_initial_file(json_file_name, n, sorted_dist_anchor, sorted_dist_anchor_dest)
        configs = [[] for i in range(n)]
        index_matrix = np.zeros((n, n), dtype=int)
        [configs, index_matrix, index_list] = generate_new_paths(x_pts, y_pts, display_range, g, path_ub, num_path,
                                                                 configs, index_matrix)

        update_configs(configs, index_list, n, ub, path_ub, path_lb, json_file_name)


    return paths


def config_deadlock(config1, config2):
    configs = [config1, config2]
    sg = Schgraph(configs)
    sg.construct()
    deadlock = sg.check_cycle()
    if deadlock:
        return -1
    else:
        return sg.makespan()


def config_priority(config1, config2):
    common_nodes = common_vertex(config1,config2)
    priority_order = []
    node_order1 = []
    node_order2 = []
    for e in common_nodes:
        order = priority_motion(config1, config2, e)
        priority_order = priority_order + order
        node_order1 = node_order1 + e[0]
        node_order2 = node_order2 + e[1]
    return node_order1, node_order2, priority_order


def generate_initial_file(filename, n, m1, m2):
    domain_table = []
    cross_table = [[[] for e in range(n)] for f in range(n)]
    two_path_dist = [[[] for e in range(n)] for f in range(n)]
    priority_table = [[[] for e in range(n)] for f in range(n)]
    path_dist = [[] for _ in range(n)]
    destination_table = [[] for _ in range(n)]
    path_list = [[] for _ in range(n)]
    edge_distance_bound = []
    ub = -1
    path_lb = -1
    path_ub = -1

    data = {
        "n": n,
        "crossTable": cross_table,
        "destinationTable": destination_table,
        "priorityTable": priority_table,
        "domains": domain_table,
        "pathTable": path_list,
        "edgeDistance": edge_distance_bound,
        "twoPathDistance": two_path_dist,
        "pathDistance": path_dist,
        "ub": ub,
        "pathLb": path_lb,
        "pathUb": path_ub,
        "anchorAnchor": m1,
        "anchorDest": m2
    }

    with open(filename, 'w') as outfile:
        json.dump(data, outfile)
    return


def generate_new_paths(x_pts, y_pts, display_range, g, sup_bound, num_path, configs, index_matrix):
    n = len(x_pts)
    index_list = []
    max_num_path = 0
    for i in range(n):
        index_list.append(len(configs[i]))
        for j in range(n):
            start = Point(x_pts[i][0], display_range[0] - x_pts[i][1])
            end = Point(y_pts[j][0], display_range[0] - y_pts[j][1])
            cand_config = g.candidate_path(start, end, sup_bound, num_path)
            if len(cand_config) > 0:
                if max_num_path < len(cand_config):
                    max_num_path = len(cand_config)
            for k in range(index_matrix[i,j],len(cand_config)):
                path = cand_config[k]
                path.append(j)
                path.append(k)
                configs[i].append(copy.deepcopy(path))
            index_matrix[i,j] = len(cand_config)
    print("max num path : ", max_num_path)
    return [configs, index_matrix, index_list]


def update_configs(configs, index_list, n, sup_bound, path_ub, path_lb, filename):
    # create priority table and the associated index table
    domain_table = []
    with open(filename) as json_file:
        data = json.load(json_file)

    cross_table = data["crossTable"]
    priority_table = data["priorityTable"]
    two_path_dist = data["twoPathDistance"]
    path_list = data["pathTable"]
    path_dist = data["pathDistance"]
    destination_table = data["destinationTable"]
    anchorAnchor = data["anchorAnchor"]
    anchorDest = data["anchorDest"]

    for i in range(n):
        paths_i = configs[i]
        for path in configs[i][index_list[i]:]:
            destination_table[i].append(path[2])
            path_dist[i].append(math.ceil(path[1]))
        domain_table.append(len(destination_table[i]))
        for j in range(n):
            if i < j:
                paths_j = configs[j]
                for p in range(index_list[i], len(paths_i)):
                    for q in range(len(paths_j)):
                        if not paths_i[p][2] == paths_j[q][2]:
                            two_path_config = config_intersection(paths_i[p][0], paths_j[q][0])
                            if two_path_config == 1:
                                ms = config_deadlock(paths_i[p][0], paths_j[q][0])
                                if 0 < ms <= sup_bound:
                                    cross_table[i][j].append([p, q, math.ceil(ms)])
                                    two_path_dist[i][j].append(math.ceil(ms))

                                    node_list1, node_list2, order_relation = config_priority(paths_i[p][0],
                                                                                             paths_j[q][0])
                                    for k in range(len(order_relation)):
                                        if order_relation[k] == 1:
                                            priority_table[i][j].append([p, node_list1[k], q, node_list2[k]])
                                        else:
                                            priority_table[j][i].append([q, node_list2[k], p, node_list1[k]])
                            elif two_path_config == 3:
                                ms = max(math.ceil(paths_i[p][1]), math.ceil(paths_j[q][1]))
                                if 0 < ms <= sup_bound:
                                    cross_table[i][j].append([p, q, math.ceil(ms)])
                                    two_path_dist[i][j].append(math.ceil(ms))

                for p in range(0, index_list[i]):
                    for q in range(index_list[j], len(paths_j)):
                        if not paths_i[p][2] == paths_j[q][2]:
                            two_path_config = config_intersection(paths_i[p][0], paths_j[q][0])
                            if two_path_config == 1:
                                ms = config_deadlock(paths_i[p][0], paths_j[q][0])
                                if 0 < ms <= sup_bound:
                                    cross_table[i][j].append([p, q, math.ceil(ms)])
                                    two_path_dist[i][j].append(math.ceil(ms))

                                    node_list1, node_list2, order_relation = config_priority(paths_i[p][0],
                                                                                             paths_j[q][0])
                                    for k in range(len(order_relation)):
                                        if order_relation[k] == 1:
                                            priority_table[i][j].append([p, node_list1[k], q, node_list2[k]])
                                        else:
                                            priority_table[j][i].append([q, node_list2[k], p, node_list1[k]])
                            elif two_path_config == 3:
                                ms = max(math.ceil(paths_i[p][1]), math.ceil(paths_j[q][1]))
                                if 0 < ms <= sup_bound:
                                    cross_table[i][j].append([p, q, math.ceil(ms)])
                                    two_path_dist[i][j].append(math.ceil(ms))

    edge_distance_bound = []

    for i in range(n):
        item1 = []
        for j in range(n):
            item2 = []
            if len(two_path_dist[i][j])>0:
                for e in two_path_dist[i][j]:
                    item2.append(e)
                item1.append([math.floor(np.min(item2)), math.ceil(np.max(item2))])
            else:
                item1.append([-1, -1])
        edge_distance_bound.append(item1)

    for i in range(n):
        for path in configs[i][index_list[i]:]:
            item2 = []
            for pt in path[0]:
                item3 = [int(pt.x), int(pt.y)]
                item2.append(item3)
            path_list[i].append(item2)

    data = {
        "n": n,
        "crossTable": cross_table,
        "destinationTable": destination_table,
        "priorityTable": priority_table,
        "domains": domain_table,
        "pathTable": path_list,
        "edgeDistance": edge_distance_bound,
        "pathDistance": path_dist,
        "twoPathDistance": two_path_dist,
        "ub": sup_bound,
        "pathLb": path_lb,
        "pathUb": path_ub,
        "anchorAnchor": anchorAnchor,
        "anchorDest": anchorDest
    }

    with open(filename, 'w') as outfile:
        json.dump(data, outfile)
    return