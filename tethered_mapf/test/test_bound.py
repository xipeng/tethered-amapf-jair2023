from utils.util import search_paths, generate_vis_graph, generate_polygons
from utils.geometry import Point as pt
from random import random
from math import sqrt
import matplotlib.pyplot as plt
import json
import pickle


def path_finding(outer_range, obstacles, departs, arrivals, opt):
    # to adapt the coordinate system
    x_pts = [(e[0], e[1]) for e in departs]
    y_pts = [(e[0], e[1]) for e in arrivals]

    # build visibility graph
    g = generate_vis_graph(obstacles, outer_range)
    # rep points are defined to compute the homotopy class
    rep_points = []
    random_offset = [random() for i in range(len(obstacles))]
    for i, polygon in enumerate(obstacles):
        sum_x = 0
        sum_y = 0
        for pg in polygon:
            sum_x = sum_x + pg[0]
            sum_y = sum_y + outer_range[1] - pg[1]
        rep_points.append(pt(sum_x / len(polygon) + (sqrt(i)/len(obstacles) + random_offset[i]*0.1)*0.5 / len(obstacles), sum_y /len(polygon) + 0.2, i + 1))

    vns_param = {
        "kmax": 7,
        "neighbor": 10,
        "vns2_num_path": 100
    }
    generate_json = opt
    paths = search_paths(x_pts, y_pts, g, rep_points, outer_range, vns_param , generate_json)
    paths = [[(e.x, outer_range[1] - e.y) for e in p] for p in paths]
    return paths


def test_simple_instance():
    '''
      input coordinate system
      -------- x
      |
      |
      |
      | y
    '''
    inputs = {
        "range": [250, 280],
        "obstacles": [[(126, 42), (140, 42), (140, 57), (126, 57)], [(115, 168), (130, 168), (130, 181), (115, 181)]],
        "departs": [(40, 20), (80, 20), (45, 220), (145, 50)],
        "arrivals": [(170, 230), (200, 230), (130, 30), (110, 170)]

    }

    outer_range = inputs["range"]
    obstacles = inputs["obstacles"]
    departs = inputs["departs"]
    arrivals = inputs["arrivals"]

    paths = path_finding(outer_range, obstacles, departs, arrivals)
    debug_visualization = True
    if debug_visualization:
        plt.figure()
        for p in obstacles:
            obs_x = [e[0] for e in p] + [p[0][0]]
            obs_y = [e[1] for e in p] + [p[0][1]]
            plt.fill(obs_x, obs_y, 'gray')
        for path in paths:
            for i in range(len(path)-1):
                plt.plot([path[i][0], path[i+1][0]], [path[i][1], path[i+1][1]], 'green')
        plt.show()


def test_bound():
    #combine oldVNS and newVNS
    display_range = [200, 200]
    map_name = "../data/map/sample10.json"
    obstacles = generate_polygons(map_name)
    simulation_file = "../data/test_alter_10/10_10_0.pickle"
    [x_pts, y_pts] = pickle.load(open(simulation_file, "rb"))
    paths = path_finding(display_range, obstacles, x_pts, y_pts, 0)
    debug_visualization = True
    if debug_visualization:
        plt.figure()
        for p in obstacles:
            obs_x = [e[0] for e in p] + [p[0][0]]
            obs_y = [e[1] for e in p] + [p[0][1]]
            plt.fill(obs_x, obs_y, 'gray')
        for path in paths:
            for i in range(len(path) - 1):
                plt.plot([path[i][0], path[i + 1][0]], [path[i][1], path[i + 1][1]], 'green')
        plt.scatter([p[0] for p in x_pts], [p[1] for p in x_pts], c='r')
        plt.scatter([p[0] for p in y_pts], [p[1] for p in y_pts], c='b')
        plt.show()


def test_optimal_solution():
    # generate json file for choco solver
    display_range = [200, 200]
    map_name = "../data/map/sample10.json"
    obstacles = generate_polygons(map_name)
    simulation_file = "../data/test_alter_10/10_10_0.pickle"
    [x_pts, y_pts] = pickle.load(open(simulation_file, "rb"))
    path_finding(display_range, obstacles, x_pts, y_pts, 1)



if __name__=="__main__":
    test_optimal_solution()