import java.util.*;

public class Connexgraph {
    private int[][][][] connexTable;
    private ArrayList<ArrayList<Path>> pathTable;
    // connected component
    private HashSet<Node> V;
    private ArrayList<ArrayList<Integer>> adj;
    private ArrayList<ArrayList<Integer>> inv_adj;


    // stack to store the visited vertices in the Topological sort
    private ArrayList<Integer> tsort;

    // Tarjan's algorithm to enumerate all the circuits
    private Stack<Integer> point_stack; // denotes the elementary path currently being considered
    private Stack<Integer> marked_stack;
    private boolean[] marked;
    private ArrayList<Map<Integer, Integer>> circuits;

    public static class Node {
        // Node is a common vertex for two paths, defined by [i, x[i], p[i]]
        // i: anchor point, x[i]: path, p[i]: the position of the shared vertex on the path
        //
        public int anchor, path, pos;
        public Node(int a1,int x1, int p1) {
            this.anchor = a1;
            this.path = x1;
            this.pos = p1;
        }
    }

    public Connexgraph(int[][][][] arr1, ArrayList<ArrayList<Path>> arr2) {
        this.connexTable = arr1;
        this.pathTable = arr2;
        this.V = new HashSet<>();
        this.tsort = new ArrayList<>();
        this.adj = new ArrayList<>();
        this.inv_adj = new ArrayList<>();
        this.point_stack = new Stack<>();
        this.marked_stack = new Stack<>();
        this.circuits = new ArrayList<>();

    }

    // initiate V and edges
    private void initiate() {
        this.V.clear();
        this.adj.clear();
        this.inv_adj.clear();
        this.tsort.clear();
        this.point_stack.clear();
        this.marked_stack.clear();
        this.circuits.clear();
        this.marked = null;

    }

    public void construct(int[] arr) {
        initiate();
        // find all the nodes based on the solution
        int n = arr.length;
        List<int[]> priority_table = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            this.V.add(new Node(i, arr[i], 0));
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    for (int k = 0; k < this.connexTable[i][j].length; k++) {
                        if(this.connexTable[i][j][k][0]==arr[i] && this.connexTable[i][j][k][2]==arr[j]) {
                            Node node1 = new Node(i, arr[i], this.connexTable[i][j][k][1]);
                            Node node2 = new Node(j, arr[j], this.connexTable[i][j][k][3]);
                            this.V.add(node1);
                            this.V.add(node2);
                            //priority_table.add(this.connexTable[i][j][k]);
                            priority_table.add(new int[] {i, arr[i], this.connexTable[i][j][k][1], j, arr[j],this.connexTable[i][j][k][3]});
                        }
                    }
                }
            }
        }


        // add edges
        int m = this.V.size();

        for(int i= 0; i < m; i++) {
            this.adj.add(new ArrayList<>());
            this.inv_adj.add(new ArrayList<>());
        }

        Node[] nodes = this.V.toArray(new Node[m]);
        // find critical nodes on each path
        ArrayList<List<Integer>> node_on_path = new ArrayList<>(n);
        for (int i=0; i<n ;i++){
            node_on_path.add(new ArrayList<>());
        }

        for (int j=0;j < m; j++){
            node_on_path.get(nodes[j].anchor).add(j);
        }
        // add priority relationship for different node on the same path
        for (int i=0; i<n ;i++){
            node_on_path.get(i).sort(Comparator.comparingInt((Integer s) -> nodes[s].pos));
            int k = node_on_path.get(i).size();
            for(int j = 0; j< k-1; j++){
                this.adj.get(node_on_path.get(i).get(j)).add(node_on_path.get(i).get(j+1));
                this.inv_adj.get(node_on_path.get(i).get(j+1)).add(node_on_path.get(i).get(j));
            }
        }
        // add priority relationship for the same node on different path
        for (int i = 0; i < m-1; i++) {
            for (int j = i+1; j < m; j++) {
                Node node1 = nodes[i];
                Node node2 = nodes[j];
                if(node1.anchor != node2.anchor) {
                    for (int[] tmp : priority_table) {
                        if (node1.anchor == tmp[0] && node1.path == tmp[1] && node1.pos == tmp[2] &&
                                node2.anchor == tmp[3] && node2.path == tmp[4] && node2.pos == tmp[5]) {
                            this.adj.get(i).add(j);
                            this.inv_adj.get(j).add(i);
                            //priority_table.remove(t);
                            break;
                        } else if (node1.anchor == tmp[3] && node1.path == tmp[4] && node1.pos == tmp[5]
                                && node2.anchor == tmp[0] && node2.path == tmp[1] && node2.pos == tmp[2]) {
                            this.adj.get(j).add(i);
                            this.inv_adj.get(i).add(j);
                            //priority_table.remove(t);
                            break;
                        }
                    }
                }
            }
        }
    }


    private void dfs(int i, boolean[] visited, Stack<Integer>recStack) {
        visited[i] = true;
        List<Integer> children = adj.get(i);
        for(Integer c: children) {
            if(!visited[c]) {
                dfs(c, visited, recStack);
            }
        }
        recStack.push(i);

    }

    public boolean check_cycle() {
        int v = this.V.size();
        boolean[] visited = new boolean[v];
        Stack<Integer> recStack = new Stack<>();

        // store the position of vertex in topological order
        Map<Integer, Integer> pos = new HashMap<>();
        int ind = 0;

        for(int i = 0; i < v; i++) {
            if (!visited[i]) {
                dfs(i, visited, recStack);
            }
        }

        // pop all elements from stack
        while (!recStack.isEmpty()) {
            pos.put(recStack.peek(), ind);
            // push element to get topological order
            this.tsort.add(recStack.peek());
            ind += 1;
            // pop from the stack
            recStack.pop();
        }

        for (int i = 0; i < v; i++) {
            for (Integer it : this.adj.get(i)) {
                // cycle exists
                if (pos.get(i) > pos.get(it)) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean on_check_cycle(boolean[] prohibited, boolean[] involved) {
        int v = this.V.size();
        for (int i = 0; i < v; i++) {
            if(!prohibited[i] && involved[i]){
                for (Integer it : this.adj.get(i)) {
                    // cycle exists
                    if(!prohibited[it] && involved[it]){
                        if(this.tsort.indexOf(i) > this.tsort.indexOf(it)){
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }


    public double makespan(int[] arr) {
        construct(arr);
        int v = this.V.size();
        double[] pass_time = new double[v];
        Node[] nodes = this.V.toArray(new Node[v]);
        double dt = 0.0;

        if(!check_cycle()) {
            // initiate passing_time with the distance from anchor point to the current point
            for(Integer i: this.tsort) {
                Path p1 = pathTable.get(nodes[i].anchor).get(nodes[i].path);
                for(Integer c: this.inv_adj.get(i)) {
                    double dist = 0.0;
                    if (nodes[i].anchor==nodes[c].anchor && nodes[i].path==nodes[c].path) {
                        dist = p1.distanceOnPath(nodes[c].pos, nodes[i].pos);
                    } else{
                        dist = dt;
                    }
                    if(pass_time[i] < pass_time[c] + dist)
                        pass_time[i] = pass_time[c] + dist;
                }
            }

            double res = 0.0;
            for(Integer i : this.tsort) {
                Path p = pathTable.get(nodes[i].anchor).get(nodes[i].path);
                pass_time[i] = pass_time[i] + p.distanceOnPath(nodes[i].pos, p.getPathSize()-1) ;
                if(pass_time[i] > res)
                    res = pass_time[i];
            }
            return res;
        }

        return -1;
    }

    public ArrayList<ArrayList<Integer>> findSubgraph(int[] arr){
        int n = arr.length;
        boolean[] visited = new boolean[n];
        boolean[] checked = new boolean[n];
        ArrayList<ArrayList<Integer>> sub_graphs = new ArrayList<>();
        for(int i=0; i<n; i++){
            visited[i] = false;
            checked[i] = false;
        }
        Deque<Integer> open_list = new ArrayDeque<>();

        for(int i=0; i<n; i++){
            open_list.clear();
            ArrayList<Integer> closed_list = new ArrayList<>();
            if(!visited[i]){
                visited[i] = true;
                open_list.add(i);
                while(!open_list.isEmpty()) {
                    int j = open_list.pop();
                    checked[j] = true;
                    closed_list.add(j);
                    for(int item=0; item <n; item++){
                        if(!visited[item]){
                            boolean has_common_vertex = false;
                            for (int k = 0; k < this.connexTable[j][item].length; k++) {
                                if(this.connexTable[j][item][k][0]==arr[j] && this.connexTable[j][item][k][2]==arr[item]){
                                    has_common_vertex = true;
                                    break;
                                }
                            }
                            for (int k = 0; k < this.connexTable[item][j].length; k++) {
                                if (this.connexTable[item][j][k][0]==arr[item] && this.connexTable[item][j][k][2]==arr[j]){
                                    has_common_vertex = true;
                                    break;
                                }
                            }
                            if(has_common_vertex){
                                open_list.add(item);
                                visited[item] = true;
                            }
                        }
                    }
                }
                sub_graphs.add(closed_list);
            }
        }
        return sub_graphs;
    }

    public double subMakespan(ArrayList<Integer> anchors){

        int v = this.V.size();
        double[] pass_time = new double[v];
        Node[] nodes = this.V.toArray(new Node[v]);
        double dt = 0.0;


        for(Integer i: this.tsort) {
            if (anchors.contains(nodes[i].anchor)) {
                Path p1 = pathTable.get(nodes[i].anchor).get(nodes[i].path);
                for (Integer c : this.inv_adj.get(i)) {
                    double dist = 0.0;
                    if (nodes[i].anchor == nodes[c].anchor && nodes[i].path == nodes[c].path) {
                        dist = p1.distanceOnPath(nodes[c].pos, nodes[i].pos);
                    } else {
                        dist = dt;
                    }
                    if (pass_time[i] < pass_time[c] + dist)
                        pass_time[i] = pass_time[c] + dist;
                }
            }
        }

        double res = 0.0;
        for(Integer i : this.tsort) {
            if(anchors.contains(nodes[i].anchor)) {
                Path p = pathTable.get(nodes[i].anchor).get(nodes[i].path);
                pass_time[i] = pass_time[i] + p.distanceOnPath(nodes[i].pos, p.getPathSize() - 1);
                if (pass_time[i] > res)
                    res = pass_time[i];
            }
        }
        return res;
    }


    ArrayList<Integer> findMaxSubgraph(int[] arr, int ms){
        ArrayList<Integer> res = new ArrayList<>();
        construct(arr);
        check_cycle();
        ArrayList<ArrayList<Integer>> sub_graphs = findSubgraph(arr);
        for(ArrayList<Integer> sg : sub_graphs){
            ArrayList<Integer> sg_paths = new ArrayList<>();
            for(int c : sg){
                sg_paths.add(arr[c]);
            }
            int m = (int) Math.ceil(subMakespan(sg));
            if(m == ms){
                return sg;
            }
        }
        return res;
    }

    private boolean backtrack(int v) {
        boolean f = false;
        point_stack.push(v);
        marked[v] = true;
        marked_stack.push(v);
        int s = marked_stack.firstElement();
        Iterator<Integer> itr = adj.get(v).iterator();
        Node[] nodes = this.V.toArray(new Node[v]);
        while(itr.hasNext()) {
            int w = itr.next();
            if (w < s) {
                itr.remove();
            }
            else if (w == s) {
                // store the circuit from s to v to s given by point stack
                Map<Integer, Integer> cycle = new HashMap<>();
                for (int element : point_stack) {
                    cycle.put(nodes[element].anchor, nodes[element].path);
                    //System.out.print(nodes[element].anchor + " "+nodes[element].path + " "+nodes[element].pos + "* ");
                }
                //System.out.println();
                if(! circuits.contains(cycle)){
                    circuits.add(cycle);
                }
                f = true;
                return f;
            }
            else if (! marked[w]) {
                f = (backtrack(w) || f);
                if(f){
                    return f;
                }
            }
        }

        if(f) {
            while(marked_stack.peek() != v) {
                int u = marked_stack.pop();
                marked[u] = false;
            }
        }
        point_stack.pop();
        return f;


    }

    public void tarjan() {
        int m = this.V.size();
        this.marked = new boolean[m];
        for(int i = 0; i < m; i++) {
            marked[i] = false;
        }

        for(int s = 0; s < m; s++) {
            boolean has_circuit = backtrack(s);
            if(has_circuit){
                break;
            }
            while(! marked_stack.isEmpty()) {
                int u = marked_stack.pop();
                marked[u] = false;
            }
        }
        //System.out.println("circuits number "+circuits.size());
    }

    public ArrayList<Map<Integer, Integer>> getCircuits(){
        // can't find minimum in this way : only by the orders relationship, beacause it could be false cycle on one path
        // (affected by the other paths). In a right way, we should recompute the topological sorting
        // we can't find the minimum, but we can find a smaller
        // findCircuitUnit();
        return circuits;
    }
}
