import com.fasterxml.jackson.databind.ObjectMapper;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.variables.IntVar;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Solver {
    private static int n; // number of robots
    private static int [] domains;
    //for all i in [0,n-1], domains[i] = array of all possible destinations for robot i
    private static int[][] destination_table;
    // for all i in [0,n-1], destination_table[i, x[i]] is the destination for robot i
    private static  Tuples[][] crossingTable;
    // for all i,j in [0,n-1], is crossingTable[i][j] contains every couple (k,l) such that path k for i crosses path l for j
    private static int[][][] cost;
    // for all i in [0,n-1], cost[i][x[i]] = cost of path x[i] for i
    private static int ub; // upper bound

    private static int path_lb; // path lower bound
    private static int path_ub; // path upper bound
    private static int [][] path_distance;

    private static Connexgraph g;

    private static int[] solution;

    private static void buildToyInstance(String file_path) {
        // read data from json file
        ObjectMapper objectMapper = new ObjectMapper();
        File file = new File(file_path);
        try {
            InputData inputs = objectMapper.readValue(file, InputData.class);
            n = inputs.getN();
            ub = inputs.getUb();
            int lb = inputs.getPathLb();
            path_lb = inputs.getPathLb();
            path_ub = inputs.getPathUb();
            path_distance = inputs.getPathDistance();
            System.out.println("upper bound = " + ub);
            int[][][][] cross_table = inputs.getCrossTable();
            cost = inputs.getEdgeDistance();
            destination_table = inputs.getDestinationTable();
            domains = inputs.getDomains();
            solution = new int[n];
            int[][][][] priority_table = inputs.getPriorityTable();
            double[][][][] path_table = inputs.getPathTable();
            crossingTable = new Tuples[n][n];

            for (int i = 0; i < cross_table.length; i++) {
                for (int j = i + 1; j < cross_table[i].length; j++) {
                    crossingTable[i][j] = new Tuples(true);
                    for (int k = 0; k < cross_table[i][j].length; k++)
                        crossingTable[i][j].add(cross_table[i][j][k][0], cross_table[i][j][k][1], cross_table[i][j][k][2]);
                }
            }

            ArrayList<ArrayList<Path>> path_list = new ArrayList<ArrayList<Path>>();
            for (int i = 0; i < path_table.length; i++) {
                ArrayList<Path> paths = new ArrayList<Path>();
                for (int k = 0; k < path_table[i].length; k++) {
                    ArrayList<Path.Point> pt_list = new ArrayList<Path.Point>();
                    for (int l = 0; l < path_table[i][k].length; l++) {
                        Path.Point node = new Path.Point(path_table[i][k][l][0], path_table[i][k][l][1]);
                        pt_list.add(node);
                    }
                    Path p = new Path(pt_list);
                    paths.add(p);
                }
                path_list.add(paths);
            }

            g = new Connexgraph(priority_table, path_list);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void lazySolver(){
        Model model = new Model();
        // declare variables and domains
        IntVar[] x = new IntVar[n]; // x[i] = path associated with robot i
        for (int i=0; i<n; i++) x[i] = model.intVar(0,domains[i]-1);

        IntVar[][] y = new IntVar[n][n]; // y[i] = length of path x[i] associated with robot i
        for (int i=0; i<n; i++){
            for (int j=i+1; j<n; j++){
                y[i][j] = model.intVar(cost[i][j][0], cost[i][j][1]);
            }
        }

        IntVar[] z = new IntVar[n]; // z[i] = destination associated with robot i
        for (int i=0; i<n; i++) z[i] = model.intVar(0, n-1);
        IntVar m = model.intVar(0, ub); // m = maximal makespan by selected paths

        // post constraints
        for (int i=0; i<n; i++) {
            model.element(z[i], destination_table[i], x[i]).post();
        }

        model.allDifferent(z).post();

        for (int i=0; i<n; i++) {
            for (int j = i + 1; j < n; j++) {
                // (x[i],x[j]) in crossingTable[i][j]
                model.table(new IntVar[]{x[i], x[j], y[i][j]}, crossingTable[i][j]).post();
                model.arithm(m, ">=",y[i][j]).post();
            }
        }

        IntVar[] d = new IntVar[n]; // d[i] = the distance of path associated with robot i
        for(int i=0; i<n; i++) d[i] = model.intVar(0, path_ub);
        IntVar max_path = model.intVar(path_lb, path_ub);
        for(int i=0; i < n; i++){
            model.element(d[i], path_distance[i], x[i]).post();
        }

        model.max(max_path,d).post();

        //solve
        org.chocosolver.solver.Solver solver = model.getSolver();
        CircuitFilter ms_constraint = new CircuitFilter(x, m, g);

        //model.setObjective(Model.MINIMIZE, m);
        solver.plugMonitor(ms_constraint);

        solver.limitTime("3600s");
        int cur_makespan=0;
        double objective = Double.POSITIVE_INFINITY;
        while (solver.solve()) {
            cur_makespan = ms_constraint.getCurMakespan();

            if (m.getValue() == cur_makespan){

                //System.out.println("makespan = " + m.getValue());
                double time_stamp = ((double) (System.currentTimeMillis()))/1000.0;
                System.out.printf("current time: %f, Max cost : %d%n", time_stamp, cur_makespan);
                solver.printShortStatistics();
                if(objective > cur_makespan){
                    objective = cur_makespan;
                }
            }else if(m.getValue() < cur_makespan){
                // find the subgraph that have this makespan
                int[] arr = new int[n];
                for(int i= 0; i < n; i++){
                    arr[i] = x[i].getValue();
                }
                ArrayList<Integer> sg = g.findMaxSubgraph(arr, cur_makespan);

                // add table constraint to forbid this combination
                int k = sg.size();
                IntVar[] new_var_list = new IntVar[k];
                int num = 0;
                int[][] t = new int[1][k];

                for (Integer i : sg) {
                    new_var_list[num] = x[i];
                    t[0][num] = x[i].getValue();
                    num++;
                }
                System.out.println("num : "+num);
                Tuples tuples = new Tuples(t,false);
                model.table(new_var_list, tuples, "FC").post();
            }
        }
        solver.printStatistics();
        System.out.println("optimal makespan = " + objective);
    }


    public static void main(String[] args){
        String file_path = args[0];
        //String file_path ="/home/xiao/Documents/PHD/work/projects/tethered_mapf/data/json_for_choco/instance_data.json";
        buildToyInstance(file_path);

        // lb solver without any heuristic
        double start_time4 = ((double) (System.currentTimeMillis()))/1000.0;
        System.out.println("lb solver start time: " + start_time4);
        lazySolver();
        double end_time4 = ((double) (System.currentTimeMillis()))/1000.0;
        System.out.println("lb solver end time: " + end_time4);

    }
}