public class InputData {
    // for cp model
    private int num_robot;

    private int[][][][] cross_table;

    private int[][][] edge_distance;

    private int [][] destination_table;

    private int [] domains;

    private int ub;

    private int path_lb;

    private int path_ub;

    // for circuit detection
    private int [][][][] priority_table;

    private double [][][][] path_table;

    private int[][] path_distance;

    private int[][][] two_path_distance;

    private int[][] anchor_anchor;

    private int[][] anchor_dest;


    // set and get methods

    public int getN() {return this.num_robot;}

    public void setN(int n) {this.num_robot = n;}

    public int[][][][] getCrossTable() {return this.cross_table;}

    public void setCrossTable(int [][][][] t) {this.cross_table = t;}

    public int[][][] getEdgeDistance() {return this.edge_distance;}

    public void setEdgeDistance(int[][][] t) {this.edge_distance = t;}

    public int[][] getDestinationTable() {return this.destination_table;}

    public void setDestinationTable(int [][] t) {this.destination_table = t;}

    public int[] getDomains() {return this.domains;}

    public void setDomains(int[] t) {this.domains = t;}

    public int [][][][] getPriorityTable() {return this.priority_table;}

    public void setPriorityTable(int[][][][] t) {this.priority_table = t;}

    public double [][][][] getPathTable() {return this.path_table;}

    public void setPathTable(double[][][][] t) {this.path_table = t;}

    public int getUb() {return this.ub;}

    public void setUb(int ub) {this.ub = ub;}

    public int getPathLb() {return this.path_lb;}

    public void setPathLb(int lb) {this.path_lb = lb;}

    public int getPathUb() {return this.path_ub;}

    public void setPathUb(int ub) {this.path_ub = ub;}

    public int[][] getPathDistance() {return this.path_distance;}

    public void setPathDistance(int [][] arr) {this.path_distance = arr;}

    public int[][][] getTwoPathDistance() {return this.two_path_distance;}

    public void setTwoPathDistance(int [][][] arr) {this.two_path_distance = arr;}

    public int[][] getAnchorAnchor() {return this.anchor_anchor;}
    public void setAnchorAnchor(int[][] arr) {this.anchor_anchor = arr;}

    public int[][] getAnchorDest() {return this.anchor_dest;}
    public void setAnchorDest(int[][] arr) {this.anchor_dest = arr;}

}

