import org.chocosolver.solver.Model;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.Solution;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;

class CircuitFilter implements IMonitorSolution {

    private final IntVar[] X ;
    private final Connexgraph g;
    private Model model;
    private IntVar ms;
    private LinkedList pool = new LinkedList<>();
    private LinkedList bests;
    int cur_makespan;


    public CircuitFilter(IntVar[] X, IntVar ms, Connexgraph g) {
        this.X = X;
        this.g = g;
        this.model = X[0].getModel();
        this.ms = ms;
        this.bests = new LinkedList<Solution>();
        this.cur_makespan = 0;
    }

    @Override
    public void onSolution() {
        int n = X.length;
        int[] solution = new int[n];
        for (int i = 0; i < n; i++) {
            solution[i] = X[i].getValue();
        }
        double makespan = g.makespan(solution);
        //System.out.println("makespan = " + makespan);
        this.cur_makespan = (int) Math.ceil(makespan);



        if (makespan < 0) {

            // find all the circuits
            //System.out.println("circuits exist");
            g.tarjan();
            ArrayList<Map<Integer, Integer>> circuits = g.getCircuits();


            for (Map<Integer, Integer> element : circuits) {
                int k = element.size();
                IntVar[] z = new IntVar[k];
                int num = 0;

                int[][] t = new int[1][k];

                for (Integer i : element.keySet()) {
                    z[num] = X[i];
                    t[0][num] = element.get(i);
                    num++;
                }
                Tuples tuples = new Tuples(t,false);
                model.table(z, tuples,"FC").post();
            }
        } else {
            // post dynamical contraint
            //int min_value = Math.min(makespan, ms.getUB());
            //model.arithm(ms, "<", min_value).post();
            model.arithm(ms, "<", this.cur_makespan).post();

        }

    }

    int getCurMakespan(){
        return this.cur_makespan;
    }

}
