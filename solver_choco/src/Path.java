import java.util.*;
import java.lang.Math;

public class Path {
    private final ArrayList<Point> p;

    public static class Point{
        private final double x;
        private final double y;

        public Point(double x, double y){
            this.x = x;
            this.y = y;
        }

        public double getX() {
            return this.x;
        }

        public double getY() {
            return this.y;
        }
    }

    public Path(ArrayList<Point> p) {
        this.p = p;
    }

    private double distanceBetweenPoints(Point pt1, Point pt2) {
        return Math.sqrt((pt1.getX() - pt2.getX())*(pt1.getX() - pt2.getX()) + (pt1.getY() - pt2.getY())*(pt1.getY() - pt2.getY()));
    }

    public double distanceOnPath(int index1, int index2) {
        // index 1 <=  index2
        double dist = 0;
        for(int i = index1; i < index2; i++)
        {
            dist = dist + distanceBetweenPoints(this.p.get(i), this.p.get(i+1));
        }
        return dist;
    }

    public double pathLength() {
        return distanceOnPath(0, this.p.size()-1);
    }

    public int getPathSize() {
        return this.p.size();
    }

}
